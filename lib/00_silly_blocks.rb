def reverser(&prc)
  sentence = prc.call
  str_arr = sentence.split(' ')
  str_arr.map(&:reverse).join(' ')
end

def adder(num=1, &prc)
  val = prc.call
  val + num
end

def repeater(cycles=1, &prc)
  cycles.times {prc.call}  
end
